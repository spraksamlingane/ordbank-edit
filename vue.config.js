
module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/lex/' : '/lex-dev/',
    devServer: {
	port: 8009,
	public: 'clarino.uib.no',
	disableHostCheck: true
    },

    runtimeCompiler: true,

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: false
      }
    },
    configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.mjs$/,
                    include: /node_modules/,
                    type: "javascript/auto"
                }
            ] 
        }
    }
}
