
export const production = process.env.NODE_ENV === 'production' ? true : false

export const apiEndpoint = production ? "https://api.ordbok.uib.no/api" : "https://clarino.uib.no/ordbok-api-dev"

// export const apiEndpoint = "https://clarino.uib.no/ordbok-api-prod"

export const apiEndpointOrdbank = production ? "https://clarino.uib.no/ordbank-api-prod" : "https://clarino.uib.no/ordbank-api-dev"

// export const ordbrukerEndpoint = "https://ordbruker.uib.no"
export const ordbrukerEndpoint = "https://oda.uib.no/ordbruker-api"

export const cacheEndpoint = "https://oda.uib.no/opal/dev/api/rebuild_cache?force=y"

// see also vue.config.js
export const lexBase = process.env.BASE_URL.slice(0,-1) // production ? '/lex' : '/lex-dev'

// Common Lisp API endpoints
export const lexLispBase = production ? '/lex' : "/lex-test"
export const entriesURL = lexLispBase + "/js/entries-json-api"
export const entryURL = lexLispBase + "/js/entry-json-api"

export const storeEntriesURL = lexLispBase +   '/js/store-entry-json-api'
export const storeImageURL = lexLispBase +   '/js/store-image-json-api'

export var editInfo = { lemmaNr: null }

import { app } from '@/main.js'

// import { store } from '@/store/index.js'

import $ from 'jquery'

import { session } from '../korpuskel/src/components/mixins/globals.js'


// import { v4 as uuidv4 } from 'uuid'
// uuidv4() // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'

export const defaultMessage = null // "Artikkelen er uendret." // doesnt work yet

var warningId = null

export const norskOrdbok = true // !production

export var displayMode = false

// if false, subentries are shown at the end of the entry, in alphabetical order
export var subentryInSitu = false

var activeCell = null

export var SID = null // $('#session-id').val()

// export var session = { sessionId: null }

var alert = { message: null, show: false }

export var checkTimer = [null, null]

export const dictNames = {
    'bl_grunnmanus': 'Artikkel med blyantrettinger',
    'bm': 'Bokmålsordboka',
    'nn': 'Nynorskordboka',
    'no': 'Norsk ordbok',
    'no_2016': 'Norsk ordbok (2016)',
    'no_ah_2005': 'Norsk ordbok A-H (2005)',
    'grunnmanus': 'Grunnmanuskriptet, utkast fra 1935',
    'hotell': 'Ordbokshotell',
    'setel': 'Setelarkivet – Norsk ordbok',
    'setel_tob': 'Setelarkivet – Trønderordboka',
    'skard': 'M. Skards ordliste',
    'torp': 'A. Torps etymologiske ordbok',
    'korpus': 'Nynorskkorpus (gammel)',
    'korpuskel': 'Nynorskkorpus',
    'mf_synopsis': 'Målføresynopsis',
    'bmnyord_seddel': 'Nyordseddel',
    'naob': 'NAOB'
}

export const wordClasses = ['prefiks',
                            'verb',
                            'substantiv',
                            'subjunksjon',
                            'interjeksjon',
                            'sammensetningsledd',
                            'determinativ (kvantor)',
                            'ukjent',
                            'determinativ (demonstrativ)',
                            'pronomen (ubestemt)',
                            'pronomen',
                            'adjektiv (ordenstall)',
                            'substantiv (egennavn)',
                            'pronomen (personlig)',
                            'pronomen (possessiv)',
                            'adverb',
                            'pronomen ??',
                            'adv.',
                            'suffiks',
                            'adjektiv',
                            'determinativ (possessiv)',
                            'preposisjon',
                            'determinativ',
                            'subst.',
                            'konjunksjon']

export const collator = new Intl.Collator('no', { // numeric: true,
                                                  sensitivity: 'base',
                                                  use: 'sort',
                                                  ignorePunctuation: true,
                                                  caseFirst: 'lower' })


const sortedStatusList = {
    'bm': [
        2, // Rediger 
        3, // Kollegalesing 
        4, // Retting 1 
        5, // Sluttkorrektur 
        6, // Retting 2 
        8, // Publiser
        9, // Utgått 
        11, // Slettet 
        12, // Skjult
        0, 1, 7, 10 // not used
    ],
    'nn': [
        2, // Rediger 
        3, // Kollegalesing 
        4, // Retting 1 
        5, // Sluttkorrektur 
        6, // Retting 2 
        8, // Publiser
        9, // Utgått 
        11, // Slettet 
        12, // Skjult
        0, 1, 7, 10 // not used
    ],
    'no': [2, 0, 1, 3, 4, 5, 6, 7, 8, 10, 13, 14, 15, 16, 17, 18, 19, 9, 11, 12]
}

export const editStateList = ["Eksisterende",
                       "Ny",
                       "Utjamning",
                       "Tom",
                       "Foreslått utgått",
                       "På vent",
                       "Ured",
                       "Foreslått skjult",
                       "Utgått"]

export const editStateListNO = ["Ny", "Redigert", "Korrekturlest", "Sletta"]

export function sortStatus (dict, c1, c2) {
    return sortedStatusList[dict].indexOf(c1) - sortedStatusList[dict].indexOf(c2)
}

export function sessionId() {
    // console.log('SID: ' + session.sessionId)
    if (session.sessionId) {
        return session.sessionId
    } else {
        loadJSON(lexLispBase + '/js/get-session-id')
            .then (r => { session.sessionId = r.sessionId
                          return session.sessionId })
            .catch (showErrorMessage)
    }
}

import { defineStore, getActivePinia, mapStores } from 'pinia'

// not persistent
export const useMOStore =
    defineStore('mo', {
        state: () => ( { key: 0,
                         editItem: null,
                         itemIndex: 0,
                         singleMode: false,
                         foc: true,
                         dictArtList: [],
                         sorter_list: [],
                         sorterPerPage: 30,
                         sorter_art_id: null,
                         showComments: true,
                         detailsRow: null,
                         searchDone: false
                       } )
    })

// used to force rebuild of the dom tree, to make arrow navigation work
// after an item is added
// but also used for other transient purposes (rename!)
export const useKeyStore =
    defineStore('key', {
        state: () => ( { key: 0,
                         node: null,
                         users: null,
                         userNames: null,
                         owners: {'bm': null, 'nn': null, 'no': null},
                         statusList: {'bm': null, 'nn': null, 'no': null},
                         info: {'bm': null, 'nn': null, 'no': null}
                       } )
    })

var ordbankStateVersion = 15

var ordbankState = JSON.parse(localStorage.getItem(process.env.BASE_URL + 'Ordbank'))

export const useOrdbankStore = defineStore('ordbank', {
    state: () => (
        ordbankState && ordbankState.version && ordbankState.version == ordbankStateVersion ?
            ordbankState :
            { version: ordbankStateVersion,
              MOArticleId: { 'mo': null, 'mo-sec': null }, // sec is secondary pane
              MOLemmaQuery: { 'mo': null, 'mo-sec': null },
              MOLemmaQueryStatus: { 'mo': null, 'mo-sec': null },
              MOTabIndex: 1,
              MOSecTabIndex: 0,
              MODictAdvancedSearch: false,
              ordbankLemmaId: { 'ob': null, 'ob-sec': null, 'mo': null, 'naob': null },
              ordbankLemmaLanguage: { 'ob': 'nob', 'ob-sec': 'nno', 'mo': 'nno', 'naob': 'nob' },
              ordbankLemmaQuery: { 'ob': null, 'ob-sec': null, 'mo': null, 'naob': null },
              ordbankQuery: { query: null,
                              language: 'nob',
                              paradigmId: null,
                              paradigmCode: null,
                              standardisation: '-',
                              caseInsensitive: false,
                              valid: true,
                              wordClass: '-',
                              source: '-'
                            },
              extendedVocab: true,
              compactDisplay: {'ob': false,
                               'ob-sec': false,
                               'mo': false,
                               'naob': false },
              dictId: null,
              naobQuery: { query: null, ambigOnly: false, variantsOnly: false, unmatchedOnly: false },
              inflectsAs: null,
              ordbokSettings: {}
            })
})

// make ordbankStore persistent in localStorage
const subscribeToOrdbankStore = async () => {
    // Wait until Pinia is initialized
    while (!getActivePinia()) {
	await new Promise((resolve) => requestAnimationFrame(resolve))
    }
    useOrdbankStore().$subscribe((mutation, state) => {
        localStorage.setItem(process.env.BASE_URL + 'Ordbank', JSON.stringify(state))
    })
}

subscribeToOrdbankStore()

var lexStateVersion = 6

var lexState = JSON.parse(localStorage.getItem(process.env.BASE_URL + 'Lex'))

export const useLexStore = defineStore('lex', {
    state: () => (
        lexState?.version && lexState.version == lexStateVersion ?
            lexState :
            { version: lexStateVersion,
              settings: { row0: true,
                          row1: true,
                          row2: true,
                          row3: false,
                          row4: false,
                          row5: false,
                          srcHide: false,
                          showOwner: true,
                          showStatus: true,
                          width0: 50,
                          width1: 50,
                          width2: 18,
                          width3: 18,
                          width4: 50,
                          width5: 50,
                        },
              dictId: 'bm',
              dictWord: [ { word: null, dictionary: 'bm' },
                          { word: null, dictionary: 'bm' } ],
              ordbankWord: { word: null, language: 'nob', extendedVocab: false },
              ordbankQuery: { query: null, language: 'nob' },
              dictQuery: { query: null, ambigOnly: false, variantsOnly: false, unmatchedOnly: false },
              moArticle: { id: null },
              dragData: null // used for cross-window drag and drop
            })
})

// make lexStore persistent in localStorage
const subscribeToLexStore = async () => {
    // Wait until Pinia is initialized
    while (!getActivePinia()) {
	await new Promise((resolve) => requestAnimationFrame(resolve))
    }
    useLexStore().$subscribe((mutation, state) => {
        localStorage.setItem(process.env.BASE_URL + 'Lex', JSON.stringify(state))
    })
}

subscribeToLexStore()


import { useStateStore } from '@/components/korpuskel/src/components/mixins/globals.js'

export function userName() {
    return useStateStore().jwt?.name || ''
}

export function personId() { // Ordbruker id
    return useStateStore().jwt?.sub || null
}

export function personRoles() { // Ordbruker roles
    return useStateStore().jwt?.roles['api.ordbok.uib.no'] || []
}

export function ordbankPersonRoles() { // Ordbruker roles
    return useStateStore().jwt?.roles['ordbank.uib.no'] || []
}


// var x = 0

// sets jwt in Lisp server
export function setJWT (mode) {
    if (localStorage.jwt) {
        loadJSON(lexLispBase + '/js/set-jwt?mode=' + mode + '&x=' + Math.random())
    }
}

var activeSetel = null

// make obsolete!
export function dictId (dict) {
    switch (dict.toLowerCase()) {
    case 'bm':
        return 'bm'
    case 'nn':
        return 'nn'
    case 'nob':
        return 'bob'
    case 'nno':
        return 'nob'
    case 'no':
        return 'no'
    case 'norm':
        return 'norm'
    }
}

export function dictLanguage (dict) {
    switch (dict) {
    case 'bm':
    return 'nob'
    case 'nn':
    return 'nno'
    case 'no':
    return 'nno' 
    }
}

export function showSetel (s) {
    if (activeSetel) {
        activeSetel.show = false
    }
    s.show = true
    activeSetel = s
}

export function posToWordclass (pos) {
    if (!pos) return ''
    pos = pos.split("_")[0]
    switch (pos) {
    case 'NOUN': return 's.'
    case 'VERB': return 'v.'
    case 'ADJ': return 'adj.'
    case 'PRON': return 'pron.'
    case 'ADV': return 'adv.'
    case 'ADP': return 'prep'
    case 'INTJ': return 'intj.'
    case 'CCONJ': return 'konj.'
    case 'SCONJ': return 'sub.'
    case 'ABBR': return 'fork.'
    case 'DET': return 'det.'
    case 'PFX': return 'prefiks'
    case 'COMPPFX': return 'forledd'
    case 'EXPR': return 'uttr.'
    case 'SYM': return 'symb.'
    default: return pos
    }
}

var _true = true

// used for Lisp API calls
export async function loadJSON (url, options) {
    // console.log('url:' + url)
    if (url.includes('?')) {
        url = url + '&jwt=' + localStorage.jwt
    } else {
        url = url + '?jwt=' + localStorage.jwt
    }
    let r= await fetch(url, options)
    if (r.ok) {
        // let c = count++
        // console.log('r:',c,r)
        r = await r.json()
        // console.log('json:',c,r)
        if (r.error == 'Session has expired.') {
            console.log("Lisp error: " + r.error)
            session.sessionId = null
        } else if (r.error) {
            console.log("Lisp error: " + r.error)
            console.log("URL: " + url)
            console.log('throw')
            throw new Error(r.error)
        } else if (r.message) {
            console.log("API error: " + r.message)
            console.log("URL: " + url)
            throw new Error("API message: " + r.message)
        }
    } else {
        console.log("API error: status = " + r.status)
        console.log("URL: " + url)
        throw new Error("HTTP status: " + r.status)
    }
    return r
 }

// used for direct API calls
// endpoint is 'ordbok' by default
// if returnPath is true the path + query component of the url returned in the response is returned.
// json and path then have to be put into an object.
// if return status is in statusList no error is generated, but the status is passed on
export async function callAPI (url, method, body, endpoint, returnLocation, params, statusList) {
    let ep, ep_url
    switch (endpoint) {
    case 'ordbank': ep = apiEndpointOrdbank
        break
    case 'absolute' : ep = '' // url is absolute
        break
    default: ep = apiEndpoint
    }
    if (params) {
        ep_url = new URL(ep + url)
        Object.entries(params).map(e => {
            ep_url.searchParams.set(e[0],e[1])
        })
    } else {
        ep_url = ep + url
    }
    let r = await fetch(ep_url,
                        { headers: {
                            "x-api-key": "niFXe2gGKw7wkJQ4jfVIV8vzgKklKZ3W6cs5b0y8", // needed only for Ordbank
                            Authorization: 'Bearer ' + localStorage.jwt,
                            "Content-Type": "application/json" }, // ??
                          method: method || "GET",
                          body
                        })
    if (r.ok) {
        if (r.status == 200) {
            r = r.json()
            if (r.message) {
                console.log("API error: " + r.message)
                console.log("URL: " + url)
                throw new Error("API message: " + r.message)
            }
        }
    } else if (statusList?.indexOf(r.status)>-1) {
        return { status: r.status }
    } else if (r.status == 404) { // new
        r = {}
    } else if (r.status >= 400) {
        console.log("API error: status = " + r.status)
        console.log("URL: " + url)
        throw new Error("API HTTP status: " + r.status)
    }
    return returnLocation ? { json: r, location: r.headers.get('Location') } : r
}

var popupComp = null

export function setPopupComp (comp) {
    popupComp = comp
}

export function closePopup () {
    // console.log('closePopup')
    if (popupComp) {
        popupComp.placePlace = null
        popupComp.litSource = null
        popupComp.referencingArticles = null
        popupComp.statusHistorie = []
        popupComp.artikkelKommentarer = []
        popupComp.sorterEntry = null
        popupComp.inTag = false
        popupComp.inEntity = false
        popupComp.inXref = false
        popupComp.showLemmaList = false
    }
    useMOStore().foc = null
}

function toggleSearch (id,value) {
    entryComps[id].hidden = !value
    // console.log('toggle')
    loadJSON('js/search-mode?session-id='
      + sessionId()
      + '&mode=visibility'
      + '&nr=' + id
      + '&hidden=' + entryComps[id].hidden)
    .then (r => {
        console.log(r)
    })
    .catch (showErrorMessage)
}

export function showErrorMessage (message) {
    app.$bvToast.toast(`${message}`, {
        title: 'Melding',
        autoHideDelay: 5000,
        // noAutoHide: true,
        appendToast: true,
        variant: 'primary',
        // solid: true,
        toaster: 'b-toaster-top-left',
        headerClass: 'toast-header', // doesnt work
        bodyClass: 'toast-body'
    })
}

export function showErrorMessageOld (message) {
    alert.message = message
    alert.show = true
}

export function dismissErrorMessage () {
    // alert.message = null
}

export function dismissErrorMessageOld () {
    alert.message = null
    alert.show = false
}

const getOffsetTop = element => {
  let offsetTop = 0;
  while(element) {
    offsetTop += element.offsetTop;
    element = element.offsetParent;
  }
  return offsetTop;
}

export function center (obj) {
    let scrollDiv = null
    function objPositionRel (obj) {
        let startObj = obj
        for (; obj && !obj.classList.contains('scroll'); obj = obj.parentNode) {
            scrollDiv = obj }
        return getOffsetTop(startObj) - getOffsetTop(scrollDiv)
    }
    let relPos = objPositionRel(obj)
    let scroll = relPos - ((document.body.clientHeight - 400) / 2)
    if (scroll < 0) {
        scroll = 0 }
    if (scrollDiv) { scrollDiv.scrollTop = scroll }
}

export function setNextDomActive () {
    // console.log('nextdomactive', app.activeNode)
    if (app.activeNode) {
        let next = nextDOMNode(app.activeNode,true,false,true,
                               next => isInput(next))
        // console.log('next:', next)
        // next?.focus()
        next?.focus({'focusVisible': true})
        app.activeNode = next
    }
}

export function setPrevDomActive () {
    // console.log('prevdomactive', app.activeNode)
    if (app.activeNode) {
        let prev = nextDOMNode(app.activeNode,false,false,true,
                               next => isInput(next))
        prev?.focus({'focusVisible': true})
        app.activeNode = prev
    }
}

export function setPrevActive () {
    let comp = app.activeComp
    if (comp) {
        let prev = nextVueNode(comp.$el,false,false,true,
                               next => next.canGetFocus)
        setActive(prev,true)
    }
}

export function isInput (elt) {
    // console.log('input?', elt.tagName, elt.type, elt.className?.includes('keyfocus'), elt)
    // console.log('className:', elt.className)
    return (elt.tagName == "TEXTAREA"
            || (elt.tagName == "INPUT"
                && (elt.type == "text" || elt.type == "number"))
            || (elt.className?.includes && elt.className?.includes('keyfocus'))
           )
}

function gotoSorterCell (dir) {
    let focusIndexKey = useMOStore().focusCell.split('-')
    let index = parseInt(focusIndexKey[0])
    let key = parseInt(focusIndexKey[1])
    let focusCell = null
    switch (dir) {
    case 'down':
        if (index < useMOStore().sorterPerPage-1) {
            focusCell = (index+1) + '-' + key
        }
        break
    case 'up':
        if (index > 0) focusCell = (index-1) + '-' + key
        break
    case 'right':
        focusCell = index + '-' + (key+1)
        break
    case 'left':
        if (key > 1) focusCell = index + '-' + (key-1)
    }
    if (focusCell) {
        unsetEditMode()
        useMOStore().foc = false
        useMOStore().focusCell = focusCell 
        useMOStore().sorterVue.$nextTick(() => {
            useMOStore().foc = true
            useMOStore().sorterVue.$nextTick(() => {
                let active = document.getElementById('text-' + focusCell)
                if (active) active.focus()
            })
        })
    }
}

function entryChanged () {
    // console.log('entryComps:')
    // console.log(entryComps)
    let changed = false
    let e = entryComps[0]
    if (e) {
        for (let i = 0; i < e.entryList.length && !changed; i++) {
            if (e.entryList[i].vue.changed && ! e.entryList[i].vue.unchanged) {
                changed = true
            }
        }
    }
    e = entryComps[1]
    if (e) {
        for (let i = 0; i < e.entryList.length && !changed; i++) {
            if (e.entryList[i].vue?.changed && !e.entryList[i].vue?.unchanged) {
                changed = true
            }
        }
    }
    return changed
}

export function handleKeyDown (event) {
    if (event.keyCode == 16) { // Shift
        // let multiStart = true
    }
    // console.log('active:', event.keyCode, document.activeElement, document.hasFocus())
    // console.log('keyCode:', event.keyCode)
    // console.log(event.ctrlKey)
    // console.log('active: ')
    // console.log(app.activeComp)
    let comp = app.activeComp
    let bgTags = ["meaning-group","bracket-group"]
    let kjTags = ["meaning-group","bracket-group"]
    let hfTags = ["meaning-group","bracket-group"]
    let box1, box2, node
    if (event.ctrlKey) {
        // console.log(event.keyCode)
        switch (event.keyCode) {
        case 84:  // T
            comp?.addNode('etymology_ref')
            event.cancelBubble = true
            event.returnValue = false
            break
        case 73:  // I
            comp.addNode && comp.addNode('etymology_lit')
            event.cancelBubble = true
            event.returnValue = false
            break
        case 68: comp.addNode && comp.addNode('definition') // D
            event.cancelBubble = true
            event.returnValue = false
            break
        case 70: comp.addNode && (comp.addNode('sub-definition') // F
                                  || comp.addNode('written_form'))
            event.cancelBubble = true
            event.returnValue = false
            break
        case 85: comp.addNode && (comp.addNode('pronunciation') // see hakeparentesGroup.vue
                                  || comp.addNode('sub-entry'))  // U
            event.cancelBubble = true
            event.returnValue = false
            break
        case 77: comp.addNode && (comp.addNode('compound-list')
                                  || comp.addNode('dialect')) // M
            event.cancelBubble = true
            event.returnValue = false
            break
        case 66: // B
            // console.log('tag = ' + comp.$options._componentTag)
            if (comp.addNode) {
                comp.addNode('meaning')
            } else if (bgTags.includes(comp.$options._componentTag)) {
                let hide = false
                let firstSeen = false
                mapComponents(
                    comp,
                    function (c) {
                        if (c.toggleBgHide) {
                            if (!firstSeen) {
                                firstSeen = true
                                hide = !c.bgHide
                            }
                            c.bgHide = hide
                        }
                    })
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 74: // J
            // console.log(comp.$options._componentTag)
            if (kjTags.includes(comp.$options._componentTag)) {
                let hide = false
                let firstSeen = false
                mapComponents(
                    comp,
                    function (c) {
                        if (c.toggleKjHide) {
                            if (!firstSeen) {
                                firstSeen = true
                                hide = !c.kjHide
                            }
                            c.kjHide = hide
                        }
                    })
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 69: comp.addNode && (comp.addNode('example') || comp.addNode('older_source')) // E
            event.cancelBubble = true
            event.returnValue = false
            break
        case 71: comp.addNode && comp.addNode('etymology_lang') // G
            event.cancelBubble = true
            event.returnValue = false
            break
            /* doesnt work on Windows
        case 86: comp.addNode && comp.addNode('etymology_ref') // V
            event.cancelBubble = true
            event.returnValue = false
            break */
        case 89: comp.addNode && comp.addNode('etymology_lang') // Y
            event.cancelBubble = true
            event.returnValue = false
            break
        case 72: // H
            if (comp.addNode) {
                comp.addNode('hp')
            } else if (hfTags.includes(comp.$options._componentTag)) {
                let hide = false
                let firstSeen = false
                mapComponents(
                    comp,
                    function (c) {
                        if (c.toggleHfHide) {
                            if (!firstSeen) {
                                firstSeen = true
                                hide = !c.hfHide
                            }
                            c.hfHide = hide
                        }
                    })
            }        
            event.cancelBubble = true
            event.returnValue = false
            break
        case 38: // arrow-up
            if (useMOStore().focusCell) {
                gotoSorterCell('up')
            } else if (app.autocomplete) {
                let prev = app.autocomplete.previousElementSibling
                if (prev) {
                    app.autocomplete.style.background='white'
                    app.autocomplete = prev
                    app.autocomplete.style.background='lightgray'
                }
            } else if (app.chooseXref) {
                // let prev = app.chooseXref.previousElementSibling
                let prev = nextDOMNode(app.chooseXref,false,false,true,
                                       next => next?.classList?.contains('xref-link-span'))
                if (prev) {
                    app.chooseXref.parentNode.style.background=null
                    app.chooseXref = prev
                    app.chooseXref.parentNode.style.background='lightgray'
                }
            } else if (comp && !comp.edit) {
                let prev = nextVueNode(comp.$el,false,false,true,
                                       prev => prev.canGetFocus)
                if (prev) {
                    setActive(prev,true)
                }
            } else if (activeCell) {
                comp = activeCell
                let next = nextVueNode(comp.$el,false,false,true,
                                       next => next.col?.position==comp.col.position)
                if (next) {
                    comp.focus = false
                    comp.edit = false
                    activeCell = next
                    activeCell.focusCell()
                }
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 40: // arrow-down
            // console.log('active:', document.activeElement, document.hasFocus())
            // console.log('fc:', useMOStore().focusCell)
            if (useMOStore().focusCell) {
                gotoSorterCell('down')
            } else if (app.autocomplete) {
                let next = app.autocomplete.nextElementSibling
                if (next) {
                    app.autocomplete.style.background=null
                    app.autocomplete = next
                    app.autocomplete.style.background='lightgray'
                }
            } else if (app.chooseXref) {
                // console.log('app.chooseXref', app.chooseXref)
                let next = nextDOMNode(app.chooseXref,true,false,false,
                                       next => next?.classList?.contains('xref-link-span'))
                // console.log(next)
                if (next) {
                    app.chooseXref.parentNode.style.background=null
                    app.chooseXref = next
                    app.chooseXref.parentNode.style.background='lightgray'
                }
            } else if (comp && !comp.edit) {
                let next = nextVueNode(comp.$el,true,false,comp.hide,
                                       next => next.canGetFocus)
                if (next) {
                    // console.log('next', next)
                    setActive(next,true)
                }
            } else if (activeCell) {
                comp = activeCell
                let next = nextVueNode(comp.$el,true,false,true,
                                       next => next.col?.position==comp.col.position)
                if (next) {
                    comp.focus = false
                    comp.edit = false
                    activeCell = next
                    activeCell.focusCell()
                }
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 37: // arrow-left <-
            // console.log('fc:', useMOStore().focusCell, 'f:', useMOStore().focusCell)
            if (useMOStore().focusCell) {
                gotoSorterCell('left')
            } else if (comp && comp.edit &&
                !app.activeNode &&
                document.activeElement &&
                isInput(document.activeElement) &&
                isAncestor(comp.$el,document.activeElement)) {
                app.activeDOM = comp.$el
                app.activeNode = document.activeElement
            }
            // console.log('yes?', comp?.edit && app.activeNode)
            if (comp?.edit && app.activeNode) {
                undoStackPush(comp)
                // go to previous edit field
                let prev = nextDOMNode(app.activeNode,false,false,true,
                                       next => isInput(next))
                // console.log('prev:', prev)
                if (prev) {
                    app.activeNode.style.background=''
                    prev.focus()
                    if (prev.type != "number" &&
                        prev.type != "button" &&
                        prev.tagName != "SELECT" &&
                        prev.setSelectionRange) {
                        prev.setSelectionRange(0,0)
                    }
                    app.activeNode = prev
                }
                
            } else if (comp) {
                let prev = nextVueNode(comp.$el,false,true,true,
                                       next => next.canGetFocus)
                if (prev) {
                    setActive(prev,true)
                    // } else {
                } else if (comp.$options._componentTag == 'dict-') {
                    comp = entryComps[0]
                    let next = nextVueNode(comp.$el,true,false,false,
                                           next => next.canGetFocus)
                    if (next) {
                        setActive(next,true)
                    }
                }
            } else if (activeCell) {
                comp = activeCell
                let next = nextVueNode(comp.$el,false,false,true,
                                       next => next.col)
                if (next) {
                    comp.focus = false
                    comp.edit = false
                    activeCell = next
                    activeCell.focusCell()
                }
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 39: // arrow-right ->
            // console.log('ae')
            // console.log(document.activeElement)
            if (useMOStore().focusCell) {
                gotoSorterCell('right')
            } else if (comp?.edit &&
                       !app.activeNode &&
                       document.activeElement &&
                       isInput(document.activeElement) &&
                       isAncestor(comp.$el,document.activeElement)) {
                app.activeDOM = comp.$el
                app.activeNode = document.activeElement
            }
            if (comp && !app.activeDOM && !app.activeNode && !activeCell) {
                app.activeDOM = comp.$el
                app.activeNode = comp.$el }
            if (comp?.edit && app.activeNode) {
                undoStackPush(comp)
                // go to next edit field
                let next = nextDOMNode(app.activeNode,true,false,false,
                                       next => isInput(next))
                if (next) {
                    app.activeNode.style.background=''
                    next.focus({'focusVisible': true})
                    if (next.type != "number" &&
                        next.type != "button" &&
                        next.tagName != "SELECT" &&
                        next.setSelectionRange) {
                        next.setSelectionRange(0,0)
                    }
                    app.activeNode = next
                }
            } else if (comp) {
                // console.log('comp')
                let next = nextVueNode(comp.$el,true,true,false,
                                       next => next.canGetFocus)
                if (next) {
                    // console.log('next')
                    setActive(next,true)
                } else if (comp.$options._componentTag == 'dict-') {
                    comp = entryComps[1]
                    next = nextVueNode(comp.$el,true,false,false,
                                       next => next.canGetFocus)
                    if (next) {
                        setActive(next,true)
                    }
                }
            } else if (activeCell) {
                comp = activeCell
                let next = nextVueNode(comp.$el,true,false,false,
                                       next => next.col)
                if (next) {
                    comp.focus = false
                    comp.edit = false
                    activeCell = next
                    activeCell.focusCell()
                }
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 13: // return CR
            // console.log('x:', app.autocomplete, app.chooseXref, displayMode, app.activeComp)
            if (app.autocomplete) {
                app.autocomplete.childNodes[0].click()
                event.cancelBubble = true
                event.returnValue = false
            } else if (app.chooseXref) {
                // was: app.chooseXref.childNodes[0].click()
                app.chooseXref.click()
                event.cancelBubble = true
                event.returnValue = false
            } else if (displayMode) {
                if (app.activeEntry) {
                    // app.activeEntry.display = true
                }
                // @@@
                if (!app.activeComp) {
                    // nothing to do
                } else if (entryChanged()) {
                    // don’t change mode if entry is changed
                } else if (isAncestorComp(app.activeEntry,app.activeComp)) {
                    if (app.activeEntry) {
                        app.activeEntry.displayMode = true
                    }
                    app.activeEntry.$nextTick(() => { // after nextTick child is dict-entry
                        setActive(app.activeEntry.$children[0])
                        app.activeEntry = null
                    })
                } else {
                    app.activeEntry = getAncestorComp('entry-wrapper', app.activeComp)
                    if (app.activeEntry) {
                        app.activeEntry.displayMode = false
                        app.activeEntry.$nextTick(() => { // after nextTick child is dict-entry
                            setActive(app.activeEntry.$children[0])
                        })
                    }
                }
                event.cancelBubble = true
                event.returnValue = false
            } else if (activeCell) {
                if (!activeCell.edit) {
                    activeCell.editCell()
                } else {
                    activeCell.edit = false
                }
                event.cancelBubble = true
                event.returnValue = false
            }
            break
        case 190: // .
        case 187: // + plus
            // console.log('comp:', comp)
            if (comp?.addSibling) {
                comp.addSibling()
            } else if (_true) {
                let active = document.activeElement
                if (!active) {
                    null
                } else if (isAncestor(comp.$el, active)) {
                    let plus = getPlusButton(active)
                    if (plus) {
                        plus.click()
                    }
                } else {
                    let plus = getLastPlusButton(comp.$el)
                    if (plus) {
                        setEditMode(comp)
                        plus.click()
                    }
                }
            } else if (document.activeElement.tagName == 'BODY') {
                let plus = getPlusButton(comp.$el)
                if (plus) {
                    setEditMode(comp)
                    plus.click()
                }
            } else {
                let plus = getPlusButton(document.activeElement)
                if (plus) {
                    setEditMode(comp)
                    plus.click()
                }
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 32:  // space
            node = useKeyStore().node
            if (node) {
                if (!node.srcHide) {
                    $("#src-input" + node.id).focus()
                    app.activeNode = $("#src-input" + node.id)[0]
                    node.srcHide = true
                } else if (!node.placeHide) {
                    $("#place-input" + node.id).focus()
                    app.activeNode = $("#place-input" + node.id)[0]
                    node.placeHide = true
                } else if (!node.attHide) {
                    $("#att-input" + node.id).focus()
                    app.activeNode = $("#att-input" + node.id)[0]
                    node.attHide = true
                }
                useKeyStore().node = null
            } else if (comp) {
                if (comp.hide) { comp.hide = false }
                toggleEditMode()
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 75: // K: toggle display mode, only if no editing has been done
            if (1 == 1 || !entryChanged()) {
                displayMode = !displayMode
                // console.log("displayMode: " + displayMode)
                entryComps[0]?.$children.map(function (c) { c.displayMode = displayMode })
                entryComps[1]?.$children.map(function (c) { c.displayMode = displayMode })
                
                if (_true || !entryComps[0].display) {
                    app.activeComp = entryComps[0] // ??
                }
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 189: // - minus: toggle hide; 191??
            if (comp) {
                // console.log(comp)
                comp.hide = !comp.hide
            }
            if (comp.hide) {
                unsetEditMode()
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 76: // L: center on page
            if (comp) {
                center(comp.$el)
            } else if (activeCell) {
                center(activeCell.$el)
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 83: // S: save
            if (comp?.top.vue?.changed) {
                comp.top.vue.storeEntry(true)
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 90: // Z: undo
            if (comp) {
                comp.top.vue.undoStackPop()
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        // case 79: // O: open/close litref, etc.
        //    break
        case 79: // O: minimize
            if (comp.top.vue.minimized) {
                comp.top.vue.minimized = false
                mapComponents(
                    comp.top.vue,
                    function (c) {
                        if (c.$options._componentTag  == 'definition-list' &&
                            !(c.node.defType=='nulltyding' || c.node.fullDefnr=='A')) {
                            c.hide = false
                        }
                    })
            } else {
                comp.top.vue.minimized = true
                mapComponents(
                    comp.top.vue,
                    function (c) {
                        if (c.$options._componentTag  == 'definition-list' &&
                            !(c.node.defType=='nulltyding' ||
                              c.node.fullDefnr=='A' || // don’t hide active comp.
                              isAncestor(c.$el, app.activeComp.$el))) {
                            c.hide = true
                        }
                    })
            }
            event.cancelBubble = true
            event.returnValue = false
            break
        case 49: // 1
            box1 = $("#article-search-1")
            box1.prop("checked", !box1.prop("checked"))
            toggleSearch(2,box1.prop("checked"))
            event.cancelBubble = true
            event.returnValue = false
            break
        case 50: // 2
            box2 = $("#article-search-2")
            box2.prop("checked", !box2.prop("checked"))
            toggleSearch(3,box2.prop("checked"))
            event.cancelBubble = true
            event.returnValue = false
            break
        }
    } else if (event.shiftKey) {
        comp = app.activeLemmaRow
        switch (event.keyCode) {
        case 38: // arrow-up
            if (comp) {
                let prev = nextVueNode(comp.$el,false,false,true,
                                       prev => prev.$options._componentTag=='entry-link')
                // console.log(prev)
                if (prev) {
                    setActiveLemmaRow(prev)
                    center(prev.$el)
                }
                event.cancelBubble = true
                event.returnValue = false
            }
            break
        case 40: // arrow-down
            if (comp) {
                let next = nextVueNode(comp.$el,true,false,true,
                                       next => next.$options._componentTag=='entry-link')
                if (next) {
                    // console.log('next:')
                    // console.log(next)
                    setActiveLemmaRow(next)
                    center(next.$el)
                }
                event.cancelBubble = true
                event.returnValue = false
            }
        }
    } else if (event.keyCode==27) { // esc
        event.cancelBubble = true
        event.returnValue = false
        // console.log('active:', document.activeElement, document.hasFocus())
        if (comp) { comp.edit = false }
        if (useMOStore().focusCell) {
            document.getElementById('text-' + useMOStore().focusCell)
                .parentElement.parentElement.parentElement.click()
            useMOStore().foc = false
            useMOStore().focusCell = null
            comp.$nextTick(() => {
                useMOStore().foc = true
            })
        }
    }
}

function isAncestor (aElt, elt) {
    elt = elt.parentNode
    while (elt) { 
        if (elt == aElt) {
            return true
        }
        elt = elt.parentNode
    }
    return false
}

function isAncestorComp (aComp, comp) {
    comp = comp.$parent
    while (comp) { 
        if (comp == aComp) {
            return true
        }
        comp = comp.$parent
    }
    return false
}

function getAncestorComp (name, comp, self) {
    if (self && comp.$options._componentTag == name) {
    return comp
    }
    comp = comp.$parent
    while (comp) { 
        if (comp.$options._componentTag == name) {
            return comp
        }
        comp = comp.$parent
    }
    return null
}

// traverses DOM nodes starting with node;
// returns node if fun(node) == true
export function nextDOMNode (node,right,flat,down,fun,root) {
    let next = null
    let ch = node.childNodes
    if (!down && !flat && ch.length) { // go up
        if (right) {
            next = ch[0]
        } else {
            next = ch[ch.length - 1]
        }
    } else if (!down) { // no children: go down
        next = node
        down = true
    } else if (right && node.nextElementSibling) { // down -> up
        next = node.nextElementSibling
        down = false
    } else if (!right && node.previousElementSibling) { // down -> up
        next = node.previousElementSibling
        down = false
    } else if (node.parentNode) { // further down
        next = node.parentNode
        down = true
    } else {
        return null
    }
    if (next == app.activeDOM) {
        return null
    } else if (root && next == root) {
        return null
    }
    if (((!down && right) || (down && !right)) && fun(next)) {
        return next
    } else {
        return nextDOMNode(next,right,flat,down,fun,root)
    }
}

// traverses the DOM to find the next Vue component in DOM order
// that satisfies fun()
function nextVueNode (node,right,flat,down,fun) {
    let next = null
    // let hide = node.hide
    let ch = node.childNodes
    if (!down && !flat && ch.length) { // go up
        if (right) {
            next = ch[0]
        } else {
            next = ch[ch.length - 1]
        }
    } else if (!down) { // no children: go down
        next = node
        down = true
    } else if (right && node.nextElementSibling) { // down -> up
        next = node.nextElementSibling
        down = false
    } else if (!right && node.previousElementSibling) { // down -> up
        next = node.previousElementSibling
        down = false
    } else if (node.parentNode) { // further down
        next = node.parentNode
        down = true
    } else {
        return null
    }
    let c = next.__vue__
    if (c && ((!down && right) || (down && !right))
        && fun(c)) {
        return c
    } else if (c && c.hide) {
        return c
    } else {
        return nextVueNode(next,right,flat,down,fun)
    }
}

function mapComponents (comp, fun) {
    fun(comp)
    comp.$children.map(function (c) { mapComponents(c,fun) })
}

export function doSubEntries (node, fun) {
    if (node) {
        Object.keys(node).map(function (key) {
            switch (key) {
            case "subEntry":
                fun(node[key])
                break
            default:
                switch (typeof(node[key])) {
                case "string":
                    break
                case "number":
                    break
                case "object":
                    doSubEntries(node[key], fun)
                }
            }
        })
    }
}


export function isAdmin () {
    let roles = personRoles()
    return (roles?.find(r=>r=="ADMIN")) && true
}

export function isOrdbankEditor (dict) {
    if (dict == 'NAOBxx') {
        return true
    } else {
        let roles = ordbankPersonRoles()
        return (roles?.find(r=>r=="EDITOR") ||
                roles?.find(r=>r=="EDITOR." + dict)) && true
    }
}

export function isEditor (dict) {
    let roles = personRoles()
    let dict_id = dictId(dict).toUpperCase()
    return (roles?.find(r=>r=="EDITOR") ||
            roles?.find(r=>r=="EDITOR." + dict_id)) && true
}

export function hasEditStatus (dict, statusCode) {
    let roles = personRoles()
    let dict_id = dictId(dict).toUpperCase()
    let statusList = useKeyStore().statusList[dict]
    let status = statusList?.find(s=>s.code==statusCode)
    return (roles?.find(r=>r=="EDITOR") ||
            roles?.find(r=>r=="EDITOR." + dict_id)) &&
        (!status?.locked ||
         roles?.find(r=>r=="WRITE_LOCKED") || // can write articles that are locked
         roles?.find(r=>r=="WRITE_LOCKED." + dict_id))
        && true
}

// write_locked = write unowned!
export function hasWriteUnownedStatus (dict) {
    let roles = personRoles()
    let dict_id = dictId(dict).toUpperCase()
    return (roles?.find(r=>r=="EDITOR") ||
            roles?.find(r=>r=="EDITOR." + dict_id)) &&
        (roles?.find(r=>r=="WRITE_LOCKED") || // can write articles that are locked
         roles?.find(r=>r=="WRITE_LOCKED." + dict_id))
        && true
}

export function hasReadStatus (dict) { // dict may also be NORM, the normalization table
    let roles = personRoles()
    let dict_id = dictId(dict).toUpperCase()
    return (roles?.find(r=>r=="READER") ||
            roles?.find(r=>r=="READER." + dict_id) ||
            roles?.find(r=>r=="EDITOR") ||
            roles?.find(r=>r=="EDITOR." + dict_id))
        && true
}

export function canAcceptDrop (comp, data) {
    // console.log('data:', data)
    return data &&
        hasEditStatus(data.dictionary) &&
        (data.dictionary == comp.top.dictionary ||
         data.dict_id == 'setel')
}

export function setEditMode (comp) {
    // console.log('setEditMode')
    closePopup()
    if (_true || hasEditStatus(comp.top.dictionary)) {
        useMOStore().focusCell = null
        if (app.activeComp) {
            if (!app.activeComp.edit) {
                undoStackPush(app.activeComp)
            }
            app.activeComp.edit = false
            app.activeComp.focus = false
            // useMOStore().foc = false
            if (useMOStore().sorterVue) {
                useMOStore().sorterVue.$nextTick(() => {
                    useMOStore().foc = true
                })
            }
        }
        comp.edit = true
        app.activeComp = comp
    }
    setFocus(comp)
    // center(comp.$el)
}

function toggleEditMode () {
    closePopup()
    // console.log('toggleEditMode:', app.activeComp)
    // console.log('act:', app.activeComp, 'can:', app.activeComp.canEdit)
    if (app.activeComp
        // && hasEditStatus(app.activeComp)
        && app.activeComp.canEdit
       ) {
        undoStackPush(app.activeComp)
        app.activeComp.edit = !app.activeComp.edit
        app.activeDOM = null
        app.activeNode = null
        app.chooseXref = null
    }
}

export function unsetEditMode () {
    closePopup()
    if (app.activeComp) {
        app.activeComp.edit = false
        undoStackPush(app.activeComp)
    }
    app.activeDOM = null
    app.activeNode = null
    app.chooseXref = null
}

export function setActive (comp, force) {
    // console.log('setActive:', comp)
    closePopup()
    if (force || !app.activeComp || !app.activeComp.edit) {
        if (app.activeComp) {
            app.activeComp.edit = false
            app.activeComp.focus = false
        }
        app.activeComp = comp
        // app.activeNode = null
        setFocus(comp)
        app.autocomplete = null
        app.autocompleteMode = null
        app.chooseXref = null
        // if (app.showLemmaList) { app.showLemmaList[0] = false }
    }
}

function setFocus (comp) {
    // console.log('setFocus', comp)
    if (app.activeComp) {
        app.activeComp.focus = false
    }
    app.activeComp = comp
    // console.log('setFocus')
    // console.log(comp)
    comp.focus = true
    comp.referencingArticles = null
    if (event) { event.cancelBubble = true }
}

export function setActiveLemmaRow (comp) {
    // closePopup()
    if (app.activeLemmaRow) {
        app.activeLemmaRow.focus = false
    }
    app.activeLemmaRow = comp
    comp.showEntry()
    comp.focus = true
    app.autocomplete = null
    app.autocompleteMode = null
    app.chooseXref = null
}

/*

Focusing of field in newly-created component:

The component needs setPendingEditMode(), focusEditMode() in mounted resp. updated.

In the code that adds a new node, app.editId has to be set to the id of the new component
using newId(), and the field that should get focus receives this id as @id in the html code.

See e.g. meaningXrefListEdit.vue and meaningListEdit.vue.

*/

export function setPendingEditMode (comp) {
    // console.log('added: ' + app.addedComp + ' componentTag: ' + comp.$options._componentTag)
    let added = null
    // in the case the definition has been lifted we have to do this trick:
    // The first definition is also re-mounted, and we have to find the correct, second one,
    // which we can identify by comparing the editId (which is the id of the embedded textarea)
    // to the id of the definition node, the difference being 2
    if (app.addedComp == 'meaning-group-lifted') {
        added = comp.$options._componentTag == 'meaning-group'
            && app.editId == comp.$options.propsData.nodes?.id - 2
    } else {
        added = app.addedComp == comp.$options._componentTag
    }
    if (added) {
        for (let c = comp; c; c = c.$parent) {
            c.hide = false
        }
        setEditMode(comp)
        app.addedComp = null
        // app.editId = null
        focusEditNode(comp)
    }
}

export function focusEditNode (comp) {
    if (app.editId) {
        let active = document.getElementById(app.editId)
        // console.log('active', active)
        if (active) {
            active.focus()
            app.activeNode = active
            app.activeDOM = comp.$el
        } // not sure why focus isn’t set first time… (and not at all on Windows)
        if (active == document.activeElement) {
            app.editId = null
        }
    }
}

function getPlusButton (plus) {
    // console.log('getPlus')
    let next = nextDOMNode(plus,true,false,false,
                           next => { return next.type == "button" || next.type=="drop" })
    if (next) {
        if (next.classList.contains('plus')) {
            return next
        } else {
            plus = next
            return getPlusButton(plus)
        }
    }
}

function getLastPlusButton (root) {
    let plus = null
    function walk (elt) { // simplify!
        let next = nextDOMNode(elt,true,false,false,
                               next => { return next.type == "button" || next.classList?.contains('plus') },
                               // next.type == "button" || next.type=="drop", // next.type == "button",
                               root)
        if (next) {
            if (next.classList.contains('plus')) {
                plus = next
            }
            elt = next
            walk(elt)
        }
    }
    walk(root)
    return plus
}

export function markChanged (comp) {
    comp.entry.changed = true
    comp.top.vue.changed = true
    comp.top.vue.stored = false
    comp.top.vue.unchanged = false
}

// we decrement newId when used to get fresh ids when needed;
// should be smaller than smallest ids assigned in Lisp code
var _newId = -1000

export function newId () {
    return _newId
}

export function getNewId () {
    return _newId--
}

// Cloning
export function cloneNode (node) {
    return JSON.parse(JSON.stringify(node))
}

// remove artikkelgruppe; replace ids by new ids
export function cloneDefinitionNode (node) {
    node = cloneNode(node)
    let idTable = new Map()
    function replaceId (str, pos) {
        let start = str.indexOf("<iref id='", pos)
        if (start > -1) {
            let end = str.indexOf("'", start + 10)
            let id = parseInt(str.substring(start+10,end))
            let nid = null
            if (idTable.get(id)) {
                nid = idTable.get(id)
            } else {
                idTable.set(id, newId)
                nid = getNewId()
            }
            return str.substring(pos, start+10) + nid.toString() + replaceId(str,end)
        } else {
            return str.substring(pos)
        }
    }
    function walk (child) {
        if (child) {
            Object.keys(child).map(function (key) {
                switch (key) {
                case "id":
                    if (idTable.get(child[key])) {
                        child[key] = idTable.get(child[key])
                    } else {
                        idTable.set(child[key], newId)
                        child[key] = getNewId()
                    }
                    break
                case "subEntries": child[key]=[]
                    break
                default:
                    switch (typeof(child[key])) {
                    case "string":
                        child[key] = replaceId(child[key],0)
                        break
                    case "number":
                        break
                    case "object":
                        walk(child[key])
                    }
                }
            })
        }
    }
    walk(node)
    return node
}

// checks whether all fields are valid
export function checkEntry (top, createLemma) {
    let entry = top.entry
    if (warningId) {
        $("#" + warningId).removeClass('warning')
    }
    let referencingArticles = top.vue.referencingArticlesCache
    let referencedDefs = []
    let entryDefs = []
    if (referencingArticles) {
        referencingArticles.map(function (// ref
        ) {
            // if (definisjonTabellId(top.dictionary) == ref.refTabellId) { // is it a def?
            // referencedDefs.push(ref)
            // }
        })
    }
    let warnings = []
    function check (string,type,id,notEmpty) {
        let res = checkConsistency(string,top.dictionary)
        if (res) {
            warnings.push({id, type, warning: res})
        }
        if (notEmpty && !string?.length) {
            warnings.push({id, type, warning: "Innhold mangler"})
        }
    }
    function walk (child,defLevel,top) {
        if (child) {
            Object.keys(child).map(function (key) {
                let node = child[key]
                if (node) {
                    switch (key) {
                    case "id":
                        break
                    default:
                        switch (typeof(node)) {
                        case "string":
                            break
                        case "number":
                            break
                        case "object":
                            if (node.type) {
                                switch (node.type) {
                                case "entry":
                                    if (!node.lemmas.length) {
                                        warnings.push({id: entry.id,
                                                       type: "lemmas",
                                                       warning: "Lemma mangler"})
                                    }
                                    break
                                case "lemma":
                                    if (node.id < 0 && !createLemma) {
                                        warnings.push({id: node.id,
                                                       type: node.type,
                                                       warning: "Lemmaet må velges fra Ordbanken"})
                                    } else if (!top && node.id < 0 && createLemma && node.lemma.indexOf(' ') == -1) {
                                        warnings.push({id: node.id,
                                                       type: node.type,
                                                       warning: "Lemmaet må være flerordsuttrykk"})
                                    }
                                    check(node.lemma,node.type,node.id,true)
                                    break
                                case "pronunciation":
                                    check(node.content,node.type,node.id,true)
                                    break
                                case "etymology-lang":
                                    check(node.intro,node.type,node.id)
                                    if (!node.language) {
                                        warnings.push({id: node.id,
                                                       type: node.type,
                                                       warning: "Ikke valgt noe språk"})
                                    }
                                    check(node.spec,node.type,node.id)
                                    break
                                case "etymology-litt":
                                    check(node.content,node.type,node.id,true)
                                    break
                                case "etymology-lit":
                                    check(node.content,node.type,node.id,true)
                                    break
                                case "etymology-ref":
                                    check(node.content,node.type,node.id)
                                    // check(node.spec,node.type,node.id)
                                    // check(node.tittel,node.type,node.id)
                                    break
                                case "definition":
                                    entryDefs.push(node.id)
                                    if (defLevel == 2 &&
                                        node.definitions.length &&
                                        node.definitions.length < 2 &&
                                        node.definitions[0].defType!='undertyding') {
                                        warnings.push({id: node.id, type: node.type,
                                                       warning: "definisjonsgruppen har bare én definisjon"})
                                    }
                                    if (1 != 1 && !node.definitions.length && !node.meanings.length
                                        && !node.subEntries.length) {
                                        warnings.push({id: node.id, type: node.type,
                                                       warning: "definisjonen mangler betydningsfeltet"})
                                    }
                                    break
                                case "meaning":
                                    check(node.text,
                                          node.type,
                                          node.id,
                                          !node.xrefs?.length && !node.usage?.length)
                                    break
                                case "example":
                                    check(node.citation,node.type,node.id)
                                    check(node.explanation,node.type,node.id)
                                    if (node.explanation && node.explanation.length
                                        && (!node.citation || !node.citation.length)) {
                                        warnings.push({id: node.id, type: node.type,
                                                       warning: "Innhold mangler"})
                                    }
                                    if ((!node.citation || !node.citation.length)
                                        && !node.xrefs.find(xref => !xref.inline)) {
                                        warnings.push({id: node.id, type: node.type,
                                                       warning: "Innhold mangler"})
                                    }
                                    break
                                case "compound-list":
                                    check(node.intro,node.type,node.id)
                                    // check(node.alternativtLedd,node.type,node.id)
                                    break
                                case "sub-entry":
                                    defLevel = 0
                                    check(node.intro,node.type,node.id)
                                    break
                                case "meaning-iref":
                                    check(node.intro,node.type,node.id)
                                    /*
                                      if (!useAPI && !node.refTabellId) {
                                      warnings.push({id: node.id,
                                      type: node.type,
                                      warning: "Ref mangler"})
                                      }
                                      xo                                    */
                                    check(node.avslutting,node.type,node.id)
                                    break
                                case "compound-xref":
                                    check(node.intro,node.type,node.id)
                                    check(node.avslutting,node.type,node.id)
                                    break
                                default:
                                    // console.log(node.type,node.type,node.id)
                                }
                            }
                            if (key=='definitions') {
                                /* let ch = child[key]?.[0]
                                   if (ch && ch.defType == 'nulltyding' && !ch.definitions?.length) {
                                   warnings.push({id: node.id,
                                   type: 'definitions',
                                   warning: "Definisjon mangler"})
                                   } */
                                walk(child[key],defLevel+1)
                            } else {
                                walk(child[key],defLevel,top)
                            }
                        }
                    }
                }
            })
        }
    }
    if (!entry.lemmas.length) {
        warnings.push({id: entry.id,
                       type: "lemmas",
                       warning: "Lemma mangler"})
    }
    /* if (!entry.definitions.length) {
       warnings.push({id: entry.id,
       type: "definitions",
       warning: "Definisjon mangler"})
       } */
    walk(entry,0,true)
    // entryDefs
    // referencedDefs
    referencedDefs.map(function (ref) {
        if (!entryDefs.find(defId => ref.refObjekt == defId)) {
            console.log('not found: ' + ref.refObjekt)
            warnings.push({id: entry.id,
                           type: "mangler",
                           warning: "slettet definisjon " + ref.refObjekt
                           + " har en referanse i artikkel " + ref.id })
        }
    })
    return warnings
}

function getTagStart (string, start) {
    let pos = string.indexOf('<',start)
    let esc = pos > 0 && string[pos-1] == '\\'
    if (esc) {
        return getTagStart(string, pos+1)
    } else {
        return pos
    }
}

// checks whether XML tags do match, and whether entities are well-formed
// returns false if everything is OK
function checkConsistency (string,dict,stack,start) {
    if (string) {
        /* if (!string || !string.length) {
           return "Tomt innhold"
           } */
        if (!start) { start = 0 }
        if (!stack) { stack = [] }
        let tagStart = getTagStart(string, start) // was: string.indexOf('<',start)
        let entityStart = string.indexOf('&',start)
        let tagType = 'start'
        if (tagStart == -1 && entityStart == -1) { // not found
            if (stack.length) {
                return "Tag ikke lukket: " + stack
            }
            return false
        } else if (tagStart > -1 && (entityStart == -1 || tagStart < entityStart)) { // Tag
            let tagEnd = string.indexOf('>',tagStart)
            let tn
            if (tagEnd == -1) {
                return "Sluttag mangler: " + string.substring(tagStart,tagStart+10) + "…"
            }
            if (string[tagStart+1] == '/') {
                tagType = 'end'
                tagStart++
            } else if (string[tagEnd-1] == '/') {
                tagType = 'empty'
                tagEnd--
            }
            let nameEnd = string.indexOf(' ',tagStart)
            if (nameEnd == -1 || nameEnd > tagEnd) {
                nameEnd = tagEnd
            }
            if (tagType == 'end' && nameEnd < tagEnd) {
                return "Sluttag inneholder for mye: <" + string.substring(tagStart,tagEnd) + ">"
            }
            let tagName = string.substring(tagStart+1, nameEnd)
            switch (tagType) {
            case 'start':
                stack.push(tagName)
                break
            case 'end':
                tn = stack.pop()
                if (!tn) {
                    return "Starttag mangler for " + tagName
                } else if (tn != tagName) {
                    return "Tagnavnene matcher ikke: " + tn + " vs. " + tagName
                }
            }
            return checkConsistency(string,dict,stack,tagEnd)
        } else if (entityStart > -1) { // Entity
            let entityEnd = string.indexOf(';',entityStart)
            if (entityEnd == -1) {
                return "Entitet ikke velformet: " + string.substring(entityStart,entityStart+10) + "…"
            }
            let entity = string.substring(entityStart,entityEnd+1)
            let entities = app.entities[dict]
            if (entities) {
                let found = false
                entities.forEach(function (ent, val) {
                    if (val === entity) {
                        found = true
                        return
                    }
                })
                if (found) {
                    return checkConsistency(string,dict,stack,entityEnd)
                } else {
                    return "Entitet ikke funnet: " + entity // string.substring(entityStart,entityEnd+1)
                }
            }
        } else {
            console.log('should not happen')
            return false
        }
    } else {
        return false
    }
}

export var entryComps = []

export function showRefEntry (dictionary,
                              definition_id,
                              article_id,
                              topEntry) {
    // console.log(article_id + ' ' + definition_id + ' ' + dictionary)
    if (!event.altKey) {
        let comp = entryComps[1] || entryComps[0]
        if (comp) {
            comp.dictionary = dictionary
            comp.showEntriesJSON(false, true, definition_id, article_id, topEntry)
            event.cancelBubble = true
        }
    }
}

export function showLitSource (comp) {
    closePopup()
    callAPI("/bibl/" + comp.node.bibl_id, null, null, 'ordbank')
        .then(r => {
            let lit = r[0]
            if (lit.author) {
                comp.litSource = lit.author + ': ' + lit.title
            } else {
                comp.litSource = lit.title
            }
        })
        .catch (showErrorMessage)
    popupComp = comp
}

export function showLitSourceOld (comp) {
    closePopup()
    loadJSON('/lex-test/js/litt-source?bibl-id=' + comp.node.bibl_id)
        .then (r =>
               {
                   let lit = r.litList[0]
                   if (lit.forfattar) {
                       comp.litSource = lit.forfattar + ': ' + lit.tittel
                   } else {
                       comp.litSource = lit.tittel
                   }
               })
        .catch (showErrorMessage)
    popupComp = comp
}

export function resolveText (dict,text,xrefs) {
    if (text?.items) {
        text = formatContent(dict, text)
    }
    // need to keep the <…>
    // split at <…> and &…;
    if (text != null && text != "") {
        let escText = text.replace(/\\\[/g, "⸦{").replace(/\\\]/g, "}⸧") // new
        // console.log(escText)
        escText = escText.replace(/\[/g, "⸦[").replace(/\]/g, "]⸧")
        escText = escText.replace(/&([^;]+);/g, function(a,b) { return "⸦&" + b + ";⸧" })
        // console.log(escText)
        escText = escText.replace(/⸦{/g, "[").replace(/}⸧/g, "]").replace(/\\</g, "<").replace(/\\>/g, ">")
        let tokens = escText.match(/[^⸦⸧]+/g)
        let i = 0
        let res = ""
        for (; i < tokens.length; i++) {
            let token = tokens[i]
            if (token.indexOf("[") === 0) {
                let id_form = token.match(/(-?\d+)\s*(\/\/)?(.+)?]$/)
                if (id_form) {
                    let id = parseInt(id_form[1])
                    let slash = id_form[2]
                    let form = slash ? id_form[3]?.trim() : null
                    let xref = xrefs?.find(x => x.id == id)
                    if (xref != null) {
                        res += "<span class='xref' "
                            + "title='" + xref.title + "' "
                            + "xref='" 
                            + dict + ","
                            + xref.definition_id + ","
                            + xref.article_id + "'>"
                        // differing markdown for sub in lemma (_) and text (__)
                            + (form || xref.form.replace(/_/g, "__"))
                            + "</span>"
                    } else {
                        res += "<span style='color: green'>??</span>"                        
                    }
                } else {
                    res += "<span style='color: green'>??</span>"
                }
            } else if (token[0] == '&') {
                let ent = app.entities[dict].get(token)
                if (ent) {
                    res += ent
                } else {
                    res += token
                }
            } else {
                res += token
            }
        }
        // console.log(res)
        return markdownToHTML(res, true, dict)
    }
}

export function markdownToHTML (str, extended, dict) {
    if (extended && dict != 'no') {
        [ ['__','sub'],
          ['_','i'],
          ['*','b'],
          ['^','sup']
        ].map(pair => str = markdownCharToHTML(str,...pair))
    } else if (extended) {
        [ ['__','sub'],
          ['_','i'], // in NO, * is used to mark poetry lines
          ['^','sup']
        ].map(pair => str = markdownCharToHTML(str,...pair))
    } else {
        [ ['_','sub'],
          ['^','sup']
        ].map(pair => str = markdownCharToHTML(str,...pair))
    }
    return str
}

function markdownCharToHTML (str,c,e,whole) {
    if (!str) return ''
    let html = ""
    let start = true
    let pos = 0
    let ee = e
    for (let i = str.indexOf(c,0);
         i >= 0;
         i = str.indexOf(c,i+1)) {
        if (start || whole) {
            html += str.substring(pos,i) + '<' + e + '>'
            if (c == '^') { // fraction
                let slash = str.indexOf('/', i+1)
                let end = str.indexOf(c,i+1)
                if (slash > -1 && slash < end) {
                    html += str.substring(i+1,slash) + '</sup>/<sub>'
                    i = slash
                    e = 'sub'
                }
            }
        } else {
            html += str.substring(pos,i) + '</' + e + '>'
            e = ee // restore value
        }
        start = !start
        pos = i + c.length
    }
    return html + str.substring(pos)
}

export function resolveLang (dict,id) {
    if (id != null && id != "") {
    return app.languages[dict].get(id)
    } else {
    return '(ikke valgt)'
    }
}

export function formatContent (dict, item) {
    let content = item ? item['content'] : null
    let id = 0
    function replaceDollar () {
        let $pos = content.indexOf('$')
        if ($pos == -1) {
            return content
        } else {
            content = content.substring(0,$pos)
                + formatItem(dict, item.items[id++])
                + content.substring($pos + 1)
            return replaceDollar()
        }
    }
    return content ? replaceDollar() : ''
}

function formatItem (dict, item) {
    let hgno
    let entities = app.entities[dict]
    switch (item.type_) {
    case "entity":
        return entities.get('&' + item.id + ';')
    case "relation":
    case "domain":
    case "grammar":
    case "rhetoric":
    case "temporal":
        return item.id
    case "pronunciation":
        return item.string
    case "article_ref":
        hgno = item.lemmas[0].hgno
        if (hgno & item.definition_order) {
            return item.lemmas[0].lemma + ' (' + toRoman(hgno) + ', ' + item.definition_order + ')'
        } else if (hgno) {
            return item.lemmas[0].lemma + ' (' + toRoman(hgno) + ')'
        } else if (item.definition_order) {
            return item.lemmas[0].lemma + ' (' + item.definition_order + 'x)'
        } else {
            return item.lemmas[0].lemma
        }
    case "usage":
        return '<ordform>' + item.text + '</ordform>'
    case "superscript":
        return '<sup>' + item.text + '</sup>'
    case "subscript":
        return '<sub>' + item.text + '</sub>'
    }
}

export function contentToMarkdown (dict, node) {
    let content = node?.content
    let id = 0
    function replaceDollar () {
        // console.log(content)
        let $pos = content.indexOf('$')
        if ($pos == -1) {
            return content
        } else {
            content = content.substring(0,$pos)
                + itemToMarkdown(dict, node.items[id++])
                + content.substring($pos + 1)
            return replaceDollar()
        }
    }
    if (content) {
        return replaceDollar()
    } else {
        return ''
    }
}

// simple version of itemify()
// escape etc. missing
export function markdownToContent(node, acc, str) {
    if (!node[acc]) {
        node[acc] = {}
    }
    let type = null
    let start = null
    let subscript = false
    let items = []
    let content = ''
    if (str) {
        for (let i = 0; i < str.length; i++) {
            let c = str[i]
            switch(c) {
            case '&':
                type = '&'
                start = i + 1
                break
            case ';':
                if (type == '&') {
                    items.push({ type_: 'entity',
                                 id: str.substring(start, i) })
                    content += '$'
                    type = null
                } else {
                    if (!type) { content += c }
                    start = i + 1
                }
                break
            case '_':
                if (subscript) {
                    subscript = false
                } else if (type == c) {
                    items.push({ type_: 'usage',
                                 text: str.substring(start, i) })
                    content += '$'
                    type = null
                } else if (type == '__' && i+1 < str.length && str[i+1] == '_') {
                    items.push({ type_: 'subscript',
                                 text: str.substring(start, i) })
                    content += '$'
                    subscript = true
                    type = null
                } else if (i+1 < str.length && str[i+1] == '_') {
                    subscript = true
                    type = '__'
                    start = i + 2
                } else {
                    type = c
                    start = i + 1
                }
                break
            case '^':
                if (type == c) {
                    items.push({ type_: 'superscript',
                                 text: str.substring(start, i) })
                    content += '$'
                    type = null
                } else {
                    type = c
                    start = i + 1
                }
                break
            default:
                if (!type) { content += c }
            }
        }
    }
    node[acc]['content'] = content
    node[acc]['items'] = items
}

// simple version of stringify()
function itemToMarkdown (dict, item) {
    let ent
    switch (item.type_) {
    case "usage":
        return '_' + item.text + '_'
    case "subscript":
        return '__' + item.text + '__'
    case "superscript":
        return '^' + item.text + '^'
    case "entity":
        return '&' + item.id + ';'        
    case "relation":
    case "domain":
    case "grammar":
    case "rhetoric":
    case "temporal":
        ent = app.entities[dict].get('&' + item.id + ';')
        return ent || item.id
    case "pronunciation":
        return item.string
    case "article_ref": // prelim
        return item
    }
}




// inline
export function formatXrefTitle (data) {
    // console.log('data')
    // console.log(data)
    // node is definition or entry; definition needs entry in addition
    let title = ""
    if (data.type == 'entry') {
        let l = data.node.lemmas[0]
        if (l) {
            title = l.lemma.replace(/\[|\]/g, "")
            console.log('title:', title)
            if (l.hgno !== 0) {
                title += " <hom>(" + toRoman(l.hgno) + ")</hom>"
            }
        }
    } else if (data.dictionary == 'no') {
        let l = data.entry.lemmas[0]
        let d = data.node
        let defnr = null
        if (d.fullDefnr) {
            defnr = formatDefPath(d.fullDefnr)
        } else {
            defnr = data.definition_nr
        }
        if (l) {
            title = l.lemma
            if (defnr == null) {
                if (l.hgno !== 0) {
                    title += " <hom>(" + toRoman(l.hgno) + ")</hom>"
                }
            } else {
                if (l.hgno !== 0) {
                    title += " <hom>(" + toRoman(l.hgno) + ", " + defnr + ")</hom>"
                } else { // can this happen?
                    title += " <hom>(" + defnr + ")</hom>"
                }
            }
        }        
    } else { // definition
        let l = data.entry.lemmas[0]
        let d = data.node
        let defnr = data.definition_nr || d.fullDefnr
        if (l) {
            title = l.lemma.replace(/\[|\]/g, "")
            console.log('title:', title)
            // title = l.lemma
            if (defnr == null) {
                if (l.hgno !== 0) {
                    title += " <hom>(" + toRoman(l.hgno) + ")</hom>"
                }
            } else {
                if (l.hgno !== 0) {
                    title += " <hom>(" + toRoman(l.hgno) + ", " + defnr + ")</hom>"
                } else { // can this happen?
                    title += " <hom>(" + defnr + ")</hom>"
                }
            }
        }
    }
    return title
}

// for NO
function formatDefPath (path) {
    let nr = ''
    if (path[0]!=null) {
        nr += String.fromCharCode(path[0]+65) // A etc.
    }
    if (path[1]!=null) {
        nr += (path[1]+1).toString() // 1 etc.
    }
    if (path[2]!=null) {
        nr += String.fromCharCode(path[2]+97) // a etc.
    }
    return nr
}

export function xrefString (data, defIndex) {
    // node is definition or entry; definition needs entry in addition
    let title = ""
    if (data.type == 'entry') {
        let l = data.node.lemmas[0]
        if (l) {
            // title = l.lemma
            title = l.lemma.replace(/\[|\]/g, "")
            if (l.hgno !== 0) {
                title += " " + toRoman(l.hgno)
            }
        }
    } else { // definition
        let l = data.entry.lemmas[0]
        let d = data.node
        let defnr = defIndex || d.fullDefnr
        if (l) {
            // title = l.lemma
            title = l.lemma.replace(/\[|\]/g, "")
            if (defnr == null) {
                if (l.hgno !== 0) {
                    title += " " + toRoman(l.hgno)
                }
            } else {
                if (l.hgno !== 0) {
                    title += " " + toRoman(l.hgno) + " " + defnr
                } else {
                    title += " " + defnr
                }
            }
        }
    }
    return title
}

// not inline, obsolete
export function formatXrefTittel2 (data) {
    // node is definition or entry; definition needs entry in addition
    let tittel = ""
    if (data.type == 'entry') {
        let l = data.node.lemmas[0]
        tittel = l.lemma
        if (l.hgno !== 0) {
            tittel = "<hom>" + toRoman(l.hgno) + "</hom> " + tittel
        }
    } else { // definition
        let l = data.entry.lemmas[0]
        let d = data.node
        tittel = l.lemma
        if (d.fullDefnr == null) {
            if (l.hgno !== 0) {
                tittel = "<hom>" + toRoman(l.hgno) + "</hom> " + tittel
            }
        } else {
            if (l.hgno !== 0) {
                tittel = " <hom>" + toRoman(l.hgno) + "</hom> " + tittel
                    + " <hom>" + d.fullDefnr + "</hom>"
            } else { // can this happen?
                tittel += " <hom>" + d.fullDefnr + "</hom>"
            }
        }
    }
    return tittel
}

export function undoStackPush (comp, change_top) {
    let top = comp.top
    if (top) {
        let str = JSON.stringify(top.entry)
        if (!top.undoStack.length || str !== top.undoStack[top.undoStack.length-1]) {
            top.undoStack.push(str)
            if (change_top) {
                comp.top.entry.changed = true
                comp.top.vue.changed = true
                comp.top.vue.stored = false
                comp.top.vue.unchanged = false
            } else {
                markChanged(comp)
            }
            // top.vue.canUndo = (top.undoStack.length > 1)
            top.vue.checkEntry()
        }
    }
}

export function toRoman (num) {
    switch (num) {
    case 0: return ""
    case 1: return "I"
    case 2: return "II"
    case 3: return "III"
    case 4: return "IV"
    case 5: return "V"
    case 6: return "VI"
    case 7: return "VII"
    case 8: return "IIX"
    case 9: return "IX"
    case 10: return "X"
    case 11: return "XI"
    case 12: return "XII"
    case 13: return "XIII"
    case 14: return "XIV"
    case 15: return "XV"
    case 16: return "XVI"
    default: return num
    }
}

export function getDefinitionIds (node) {
    let ids = []
    function walk (child) {
    if (child) {
        Object.keys(child).map(function (key) {
        if (key == 'id') {
            if (child['type'] == 'definition') {
            ids.push(child[key])
            }
        } else if (typeof(child[key]) == 'object') {
            walk(child[key])
        }
        })
    }
    }
    walk(node)
    return ids
}

export function setTemplateIds (entry) {
    let clone = cloneNode(entry)
    let tempId = -1
    let idTable = new Map()
    function getId (id) {
        if (idTable.get(id)) {
            return idTable.get(id)
        } else {
            idTable.set(id, tempId--)
            return idTable.get(id)
        }
    }
    function replaceId (str, pos) {
        let start = str.indexOf("<iref id='", pos)
        if (start > -1) {
            let end = str.indexOf("'", start + 10)
            let id = parseInt(str.substring(start+10,end))
            let nid = null
            if (idTable.get(id)) {
                nid = idTable.get(id)
            } else {
                idTable.set(id, tempId)
                nid = tempId--
            }
            return str.substring(pos, start+10) + nid.toString() + replaceId(str,end)
        } else {
            return str.substring(pos)
        }
    }
    function walk (child) {
        if (child) {
            Object.keys(child).map(function (key) {
                if (key == 'id') {
                    if (child[key] > 0) {
                        child[key] = getId(child[key])
                    }
                } else if (typeof(child[key]) == 'string') {
                    child[key] = replaceId(child[key])
                } else if (typeof(child[key]) == 'object') {
                    walk(child[key])
                }
            })
        }
    }
    walk(clone)
    clone.redaktor = null
    clone.redaktorId = null
    return clone
}

export function reorderDrop (comp, data) {
    comp.over = false
    console.log(data.entry == comp.entry, data.node.type, comp.node.type)
    // check if same entry and type
    if (data.entry == comp.entry && // was: comp.top.entry
        data.node.type == comp.node.type) {
        let dragIndex = data.nodes.indexOf(data.node)
        data.nodes.splice(dragIndex,1) // remove dragged node
        let dropIndex = comp.nodes.indexOf(comp.node)
        
        if (dropIndex==dragIndex-1) {
            comp.nodes.splice(dropIndex,0,data.node) // insert dragged node past target
        } else {
            comp.nodes.splice(dropIndex+1,0,data.node) // insert dragged node past target
        }
        undoStackPush(comp)
    }
}

export function showMORefEntry (refTabellId, refObjekt) {
    if (!event.altKey) {
    let comp = entryComps[1]
    comp.dictionary = 'mo'
    comp.showMOEntryJSON(false, true, refTabellId, refObjekt)
    event.cancelBubble = true
    }
}

export const lexCommon = {
    data () {
        return {
            alert
        }
    },
    computed: {
        ...mapStores(useLexStore, useKeyStore),
        defaultMessage: function () {
            return defaultMessage
        }
    }
}
