
import { apiEndpointOrdbank } from './ordbokUtils.js'

function loadJSON (url) {
    return fetch(url, { headers: { "x-api-key": "niFXe2gGKw7wkJQ4jfVIV8vzgKklKZ3W6cs5b0y8" } })
        .then(r => r.json())
        .then(r => {
            if (r.error) {
                throw new Error(r.error + ", URL: " + url)
            }
            return r
        })
}

var editInfo = { lemmaNr: null }

export const api = {
    data () {
        return {
            editInfo,
            apiURL: apiEndpointOrdbank
        }
    },
    methods: {
        showAlert (message) {
            console.log('Alert:')
            console.log(message)
            this.alert.message = message
            this.alert.show = true
        },
        callAPI (command, params) { // make obsolete!
            let url = this.apiURL + command
            if (params) {
                Object.keys(params).map(function(key) {
                    
                    url += '&' + key + '=' + encodeURIComponent(params[key]) })
            }
            return loadJSON(url).then(r => { return r })
        }
    }
}
