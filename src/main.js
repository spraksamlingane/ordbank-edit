import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import VueRouter from 'vue-router'
import './plugins/bootstrap-vue'
import App from './App.vue'
import lexMain from './components/lexMain.vue'

import ordbankSearch from './components/ordbank/ordbankSearch.vue'
import ordbankLemmaWrapper from './components/ordbank/ordbankLemmaWrapper.vue'
import paradigmDefinition from './components/ordbank/paradigmDefinition.vue'
import lemmaSearch from './components/ordbank/lemmaSearch.vue'

import ordbokSearch from './components/ordbok/ordbokSearch.vue'

import normalizationTable from './components/ordbok/normalizationTable.vue'

import corpusSearch from './components/korpuskel/src/components/corpusSearch.vue'
import corpusList from './components/korpuskel/src/components/corpusList.vue'
import corpusDetails from './components/korpuskel/src/components/corpusDetails.vue'


import lexAdmin from './components/admin/lexAdmin.vue'
import lexDocumentation from './components/lexDocumentation.vue'

import metaOrdbok from './components/ordbank/metaOrdbok.vue'
import moArticle from './components/ordbank/moArticle.vue'
import moSearch from './components/ordbank/moSearch.vue'
import moBibliography from './components/ordbank/moBibliography.vue'
import moPlaces from './components/ordbank/moPlaces.vue'
import moRegistrants from './components/ordbank/moRegistrants.vue'

import naob from './components/ordbank/naob.vue'
import naobSearch from './components/ordbank/naobSearch.vue'


import $ from 'jquery'

// import { store } from '@/store/index.js'

import i18n from '@/plugins/i18n'

import { norskOrdbok, loadJSON, callAPI, setJWT,
         showErrorMessage, sessionId, // ordbrukerEndpoint,
         lexLispBase, userName
       } from './components/mixins/ordbokUtils.js'

Vue.config.productionTip = false

Vue.use(VueRouter)

/* 
// see korpuskel/src/components/mixin/globals.js
function parse_jwt(jwt) {
    let base64Url = jwt.split('.')[1]
    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    let jsonPayload = decodeURIComponent(atob(base64)
                                         .split('')
                                         .map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
                                         .join(''))

    return JSON.parse(jsonPayload)
}
*/

import { createPinia, PiniaVuePlugin,
         //  getActivePinia
       } from 'pinia'

Vue.use(PiniaVuePlugin)

const pinia = createPinia()

const router = new VueRouter({
    mode: 'history',
    base: "/",
    // history: createWebHistory(),
    routes: [
        {
            path: '/:project/login-jwt',
            redirect: to => {
                let jwt = to.query['jwt']
                let path = to.query['path']
                localStorage.jwt = jwt
                setJWT('redirect') // strangely this is necessary!
                return { path,
                         query: null }
            }
        },
        {
            path: '/:project/login',
            redirect: to => {
                let jwt = to.query['jwt']
                let path = to.query['path']
                localStorage.jwt = jwt
                setJWT('redirect') // strangely this is necessary!
                return { path,
                         query: null }
            }
        },
        {
            path: '/:project',
            redirect: '/:project/home'
        },
        {
            name: 'main',
            path: '/:project/home',
            component: lexMain
        },
        {
            name: 'root',
            path: '/:project',
            component: lexMain,
            children:
            [
                {
                    // name: 'ordbank',
                    path: 'ordbank',
                    component: ordbankSearch,
                    props: true,
                    children: [ { // name: 'ordbank',
                                  path: 'lemma/:id',
                                  component: ordbankLemmaWrapper,
                                  props: true },
                                { name: 'ordbank',
                                  path: 'lemma',
                                  component: ordbankLemmaWrapper,
                                  props: true },
                                { path: '',
                                  component: ordbankLemmaWrapper,
                                  props: true },
                                { name: 'ordbank-list',
                                  path: 'list',
                                  component: lemmaSearch,
                                  props: true },
                                { name: 'ordbank-paradigm',
                                  path: 'paradigm/:id',
                                  component: paradigmDefinition,
                                  props: true },
                                { // name: 'ordbank-paradigm',
                                    path: 'paradigm',
                                    component: paradigmDefinition,
                                    props: true }
                              ]
                },
                {
                    path: 'metaordbok',
                    component: metaOrdbok,
                    props: true,
                    children: [ { name: 'metaordbok',
                                  path: 'article',
                                  component: moArticle,
                                  props: () => ({ mode: 'mo' }) },
                                { path: 'article/:id',
                                  component: moArticle,
                                  props: route => ({ id: route.id,
                                                     mode: 'mo' } )
                                },
                                { path: '',
                                  component: moArticle,
                                  props: () => ({ mode: 'mo' }) },
                                { name: 'metaordbok',
                                  path: 'search',
                                  component: moSearch,
                                  props: () => ({ mode: 'mo' }) },
                                { path: 'bibl',
                                  component: moBibliography,
                                  props: true },
                                { path: 'bibl/:id',
                                  component: moBibliography,
                                  props: true },
                                { path: 'places',
                                  component: moPlaces,
                                  props: true },
                                { path: 'places/:id',
                                  component: moPlaces,
                                  props: true },
                                { path: 'registrants',
                                  component: moRegistrants,
                                  props: true },
                                { path: 'registrants/:id',
                                  component: moRegistrants,
                                  props: true }
                              ]
                },
                {
                    path: 'naob',
                    component: naob,
                    props: true,
                    children: [ { name: 'naobSearch',
                                  path: 'article',
                                  component: naobSearch,
                                  props: true },
                                { path: '',
                                  component: naobSearch,
                                  props: true },
                                /*
                                { path: 'ordbank/lemma/:id',
                                  component: ordbankLemmaWrapper,
                                  props: true }
                                  */
                              ]
                },
                /*{
                    name: 'ordbank',
                    path: 'ordbank',
                    component: ordbankSearch,
                    props: true },*/
                {
                    name: 'ordbok',
                    path: 'ordbok',
                    component: ordbokSearch,
                    props: true },
                {
                    name: 'normal',
                    path: 'normal',
                    component: normalizationTable,
                    props: true },
                {
                    name: 'corpus-query',
                    path: 'corpus/:bag/:query',
                    component: corpusSearch,
                    props: true },
                {
                    name: 'text',
                    path: 'text/:corpus?/:document?',
                    // component: textPageMenota
                    component: () => import('./components/korpuskel/src/components/textPageKorpuskel.vue')
                },
                {
                    path: 'corpus',
                    redirect: 'corpus/concordance'
                    /* path: 'corpus',
                       component: corpusSearch,
                       props: true */
                },
                {
                    name: 'corpus-tab',
                    path: 'corpus/:view',
                    component: corpusSearch
                },
                {
                    name: 'details',
                    path: 'details',
                    component: corpusDetails,
                    props: route => ({ collections: route.query.collection,
                                       languages: route.query.language,
                                       bag: route.query.corpus })
                },
                {
                    name: 'corpora',
                    path: 'corpora',
                    component: corpusList,
                    props: true },
                {
                    name: 'admin',
                    path: 'admin',
                    component: lexAdmin,
                    props: true },
                {
                    name: 'documentation-start',
                    path: 'documentation',
                    component: lexDocumentation,
                    props: true },
                {
                    name: 'documentation',
                    path: 'documentation/:page',
                    component: lexDocumentation,
                    props: true }

            ]
        }
    ],
})

/*
router.afterEach((to, from, next) => {
    console.log(to)
    console.log(from)
    console.log(next)
})
*/

/*
function trunk16 (str) {
    return str.length < 17 ? str : str.substring(0,16) + "…"
}
*/

export const apiEndpointKorpuskel =
      process.env.NODE_ENV === 'production' ? "https://clarino.uib.no/lex/" : "https://clarino.uib.no/lex-test/"

export const app = new Vue({
    router,
    i18n,
    pinia,
    // store,
    render: h => h(App),
    // el: '#ordbok-table',
    data: { // personId: userNameToPersonId(userName()), // $('#person-id').val(),
        editor: $('#editor').val(),
        word: ['hyse','mus'],
        dictionary: ['bm','nn'],
        activeComp: null, // the highlighted comp
        activeDOM: null, // root DOM of active comp
        activeEntry: null, // the active entry comp when in display mode
        activeNode: null,
        plusNode: null,
        addedComp: null,
        editId: null, // is set on plus click; used to put new input field in focus
        autocomplete: null,
        autocompleteMode: null,
        chooseXref: null,
        // showLemmaList: null,
        // undoStack: [[],[]],
        currentLemma: null, // test, used in lemmaListEdit.vue
        activeLemmaRow: null, // the active lemma in search results
        entities: [],
        tags: [], // should be obsolete? ["<ordform>", "<SUP>", "<SUB>", "<FRAK>"],
        languages: [],
        langArray: [],
        permissions: [],
        statusList: [],
        info: [],
        placeList: null,
        placeArray: null,
        altHeim: null,
        subcat: null,
        slipMorphCodes: null,
        usages: [],
        // userNames: null,
        // users: null,
        // personIds: [],
        // entries: [{ 'id': 0 }, { 'id': 1 }],
        documentationEditMode: false
    },
    created: function () {
        setJWT('created')
        sessionId()
        this.getEntities("bm")
        this.getEntities("nn")
        this.getLanguages("bm")
        this.getLanguages("nn")
        this.getUsages("bm")
        this.getUsages("nn")
        // this.getInfo("bm")
        // this.getInfo("nn")
        if (localStorage.jwt) {
            this.getPermissions('bm')
            this.getPermissions('nn')
            if (norskOrdbok) { this.getPermissions('no') }
            // this.getUsers()
        }
        if (norskOrdbok) {
            this.getEntities("no")
            this.getLanguages("no")
            this.getUsages("no")
            this.getPlaceList()
            this.getSubcat()
            this.getSlipMorphCodes()
        }
    },
    methods: {
        // obsolete
        getInfo: function (dict) {
            if (userName()) {
                callAPI('/dict/' + dict)
                    .then (r => this.info[dict] = r)
                    .catch (showErrorMessage)
            }
        },
        getPermissions: function (dict) {
            callAPI('/dict/' + dict + '/permission')
                .then (r => this.permissions[dict] = r)
                .catch (showErrorMessage)
            // console.log('getPermissions')
            callAPI('/dict/' + dict + '/status')
                .then (r => this.statusList[dict] = r)
                .catch (showErrorMessage)
        },
        getEntities: function (dict) {
            if (!this.entities[dict]) {
                loadJSON(lexLispBase + '/js/entities-json?dictionary=' + dict)
                    .then (response =>
                           { let map = new Map()
                             let ent = response.entities
                             for (let i = 0; i < ent.length; i += 2) {
                                 map.set(ent[i], ent[i + 1])
                             }
                             let omap = new Map([...map.entries()].sort((a, b) => a[0].localeCompare(b[0])))
                             this.entities[dict] = omap
                           })
                    .catch (showErrorMessage)
            }
        },
        getUsages: function (dict) {
            if (!this.usages[dict]) {
                loadJSON(lexLispBase + '/js/usages-json?dictionary=' + dict)
                    .then (response => this.usages[dict] = response.usages)
                    .catch (showErrorMessage)
            }
        },
        // obsolete
        /*
        getUsers: function () { // oracle-id -> alias
            let url = ordbrukerEndpoint + '/admin?jwt=' + localStorage.jwt
            loadJSON(url)
                .then(r => {
                    let map = new Map()
                    let users = r.users
                    for (let i = 0; i < users.length; i++) {
                        users[i]._showDetails = false
                        map.set(users[i].id, users[i])
                    }
                    this.users = map
                    this.userNames = Array.from(this.users,u=>u[1])
                        .sort((a,b)=>a.name<b.name?-1:1)
                        .map(u => { return { value: u.id, text: trunk16(u.name) } })
                    // console.log(map)
                })
                .catch (showErrorMessage)
        },*/
        getLanguages: function (dict) {
            if (!this.languages[dict]) {
                loadJSON(lexLispBase + '/js/languages-json?dictionary=' + dict)
                    .then (response =>
                           { let map = new Map()
                             let lang = response.languages
                             for (let i = 0; i < lang.length; i += 2) {
                                 map.set(lang[i], lang[i + 1])
                             }
                             this.languages[dict] = map
                             let langArray = Array.from(this.languages[dict])
                             langArray.sort(function(a,b) {return a[1] < b[1] ? -1 : 1})
                             this.langArray[dict] = langArray
                           })
                    .catch (showErrorMessage)
            } },
        getPlaceList: function () {
            if (!this.placeList) {
                callAPI("/place?place_name=" + encodeURIComponent('%'), null, null, 'ordbank')
                    .then(r => {
                        this.placeList = r
                        // console.log(r)
                        // console.log(Array.from(Object.values(this.placeList)))
                        let placeArray = Array.from(Object.values(this.placeList))
                        placeArray.sort(function(a,b) {
                            return a.name_full < b.name_full ? -1 : 1
                        })
                        this.placeArray = placeArray
                    })
                    .catch (showErrorMessage)
            }
        },
        getAltHeim: function () { // not used!
            if (!this.altHeim) {
                loadJSON(lexLispBase + '/js/alt-heim-json')
                    .then (response =>
                           { this.altHeim = response.altHeim })
                    .catch (showErrorMessage)
            }
        },
        getSubcat: function () {
            if (!this.subcat) {
                loadJSON(lexLispBase + '/js/subcat-json')
                    .then (response =>
                        { this.subcat = response.subcat })
                    .catch (showErrorMessage)
            }
        },
        getSlipMorphCodes: function () {
            if (!this.slipMorphCodes) {
                // console.log('get morph_codes')
                callAPI('/dict/setel/morph_codes', 'GET', null, 'ordbank')
                    .then(r => this.slipMorphCodes = r)
            .catch (showErrorMessage)
            }
        }
    }
})

// app has to be available before the component is mounted!
app.$mount('#app')
